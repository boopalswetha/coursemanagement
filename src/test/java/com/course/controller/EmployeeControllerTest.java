package com.course.controller;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.course.dto.EnrollResponseDto;
import com.course.dto.TrainingResponseDto;
import com.course.model.Batch;
import com.course.model.Course;
import com.course.model.Employee;
import com.course.service.EmployeeService;
import com.course.service.EmployeeTrainingService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EmployeeControllerTest {
	 @InjectMocks
	 EmployeeController employeeController;
	    
	    @Mock
	    EmployeeService employeeService;
	   
	    public void testenrollTrainingForPositive() {
	    	Course course = new Course();
			course.setCourseId((long) 55);
			course.setCourseName("boopal");
			
			List<Employee> emp=new ArrayList<>();
			Employee employee=new Employee();
			employee.setEmail("boopal@gmail.com");
			employee.setEmployeeId(1L);
			employee.setEmployeeName("boopal");
			employee.setPhone("6475897");
			employee.setRedgStatus("enroll");
			emp.add(employee);
			
			List<Batch> batchs=new ArrayList<>();
			Batch batch = new Batch();
			batch.setBatchId((long) 55);
			batch.setBatchname("superclass");
			batch.setCourse(course);
			batch.setStartDate(LocalDate.now());
			batch.setEndDate(LocalDate.of(2020,06,25));
			batch.setEmployee(emp);
			batchs.add(batch);
			Mockito.when(employeeService.enrollCourse(Mockito.anyLong(),Mockito.anyLong())).thenReturn(null);
			ResponseEntity<EnrollResponseDto> result= employeeController.enrollTraining(1L, 2L);
			assertNotNull(result);
	    }
	    public void testenrollTrainingForNagative() {
	    	Course course = new Course();
			course.setCourseId((long) 55);
			course.setCourseName("boopal");
			
			List<Employee> emp=new ArrayList<>();
			Employee employee=new Employee();
			employee.setEmail("boopal@gmail.com");
			employee.setEmployeeId(-1L);
			employee.setEmployeeName("boopal");
			employee.setPhone("6475897");
			employee.setRedgStatus("enroll");
			emp.add(employee);
			
			List<Batch> batchs=new ArrayList<>();
			Batch batch = new Batch();
			batch.setBatchId((long) 55);
			batch.setBatchname("superclass");
			batch.setCourse(course);
			batch.setStartDate(LocalDate.now());
			batch.setEndDate(LocalDate.of(2020,06,25));
			batch.setEmployee(emp);
			batchs.add(batch);
			Mockito.when(employeeService.enrollCourse(null,null)).thenReturn(null);
			ResponseEntity<EnrollResponseDto> result= employeeController.enrollTraining(1L, 2L);
			assertNotNull(result);
	    }
	    }
