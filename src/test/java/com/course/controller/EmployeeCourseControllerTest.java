package com.course.controller;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.course.dto.EnrollResponseDto;
import com.course.dto.TrainingResponseDto;
import com.course.model.Batch;
import com.course.model.Course;
import com.course.model.Employee;
import com.course.service.EmployeeCourseService;
import com.course.service.EmployeeService;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EmployeeCourseControllerTest {

	 @InjectMocks
	 EmployeeCourseController employeeController;
	    
	    @Mock
	    EmployeeCourseService employeeTrainingService;
	    
	    @Test
	    public void testgetAllPositive() {


			Course course = new Course();
			course.setCourseId((long) 55);
			course.setCourseName("boopal");
			
			List<Employee> emp=new ArrayList<>();
			Employee employee=new Employee();
			employee.setEmail("boopal@gmail.com");
			employee.setEmployeeId(1L);
			employee.setEmployeeName("boopal");
			employee.setPhone("6475897");
			employee.setRedgStatus("enroll");
			emp.add(employee);
			
			List<Batch> batchs=new ArrayList<>();
			Batch batch = new Batch();
			batch.setBatchId((long) 55);
			batch.setBatchname("superclass");
			batch.setCourse(course);
			batch.setStartDate(LocalDate.now());
			batch.setEndDate(LocalDate.of(2020,06,25));
			batch.setEmployee(emp);
			batchs.add(batch);
			Mockito.when(employeeTrainingService.getAll(Mockito.anyString())).thenReturn(null);
			ResponseEntity<List<TrainingResponseDto>> result= employeeController.getTraining(null);
			assertNotNull(result);
	    }
	    public void testgetAllNagative() {


			Course course = new Course();
			course.setCourseId((long) 55);
			course.setCourseName("boopal");
			
			List<Employee> emp=new ArrayList<>();
			Employee employee=new Employee();
			employee.setEmail("boopal@gmail.com");
			employee.setEmployeeId(-1L);
			employee.setEmployeeName("boopal");
			employee.setPhone("6475897");
			employee.setRedgStatus("enroll");
			emp.add(employee);
			
			List<Batch> batchs=new ArrayList<>();
			Batch batch = new Batch();
			batch.setBatchId((long) 55);
			batch.setBatchname("superclass");
			batch.setCourse(course);
			batch.setStartDate(LocalDate.now());
			batch.setEndDate(LocalDate.of(2020,06,25));
			batch.setEmployee(emp);
			batchs.add(batch);
			Mockito.when(employeeTrainingService.getAll(Mockito.anyString())).thenReturn(null);
			ResponseEntity<List<TrainingResponseDto>> result= employeeController.getTraining(null);
			assertNotNull(result);
	    }
}
