package com.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.course.dto.EnrollResponseDto;
import com.course.service.EmployeeService;
@RestController
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;

	@PostMapping("/enroll")
	public ResponseEntity<EnrollResponseDto> enrollTraining(@RequestParam Long employeeId ,@RequestParam Long batchId) {
		EnrollResponseDto enrollResponseDto = employeeService.enrollCourse(employeeId,batchId);
		return new ResponseEntity<>(enrollResponseDto, HttpStatus.OK);
	}

}
