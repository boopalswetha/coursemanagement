package com.course.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.course.dto.TrainingResponseDto;
import com.course.service.EmployeeCourseService;

@RestController
public class EmployeeCourseController {
	
	@Autowired
	EmployeeCourseService employeeTrainingService;

	@GetMapping("/search")
	public ResponseEntity<List<TrainingResponseDto>> getTraining(@RequestParam String courseName) {
		List<TrainingResponseDto> trainingResponseDtos = employeeTrainingService.getAll(courseName);
		return new ResponseEntity<>(trainingResponseDtos, HttpStatus.OK);
	}

}
