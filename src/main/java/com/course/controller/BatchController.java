package com.course.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.course.model.Batch;
import com.course.service.EmployeeTrainingService;


@RestController
public class BatchController {
	@Autowired
	EmployeeTrainingService employeeTrainingService;
	
	@GetMapping("/course")
	public ResponseEntity<List<Batch>>getCourses(@RequestParam String courseName){
		List<Batch>	batchs=employeeTrainingService.getAll(courseName);
		return new ResponseEntity<>(batchs,HttpStatus.OK);
	}

	
	
	

}
