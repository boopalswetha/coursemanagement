package com.course.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.model.Batch;
import com.course.model.Course;
import com.course.repository.BatchRepository;
import com.course.repository.CourseRepository;



@Service
public class EmployeeTrainingService {
	
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	BatchRepository batchRepository;
	
	public List<Batch> getAll(String courseName){
		Course course=courseRepository.findByCourseName(courseName);
		List<Batch>list=batchRepository.findByCourse(course);
		List<Batch> compareTo=list.stream().sorted((i1,i2)->-i2.getStartDate().compareTo(i1.getStartDate())).collect(Collectors.toList());
		return compareTo;
		
	}

	public Batch save(Batch batch) {
		
		return batchRepository.save(batch);
	}
	

}
