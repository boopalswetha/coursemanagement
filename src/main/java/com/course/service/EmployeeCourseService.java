package com.course.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.dto.TrainingResponseDto;
import com.course.exception.DataNotFoundException;
import com.course.model.Batch;
import com.course.model.Course;
import com.course.repository.BatchRepository;
import com.course.repository.CourseRepository;

/**
 * 
 * @author saikrishna
 *
 */
@Service
public class EmployeeCourseService {

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	BatchRepository batchRepository;

	public List<TrainingResponseDto> getAll(String courseName) {

		Course course = courseRepository.findByCourseName(courseName);
		LocalDate date = LocalDate.now();
		List<Batch> list = batchRepository.findByCourseAndStartDateGreaterThanEqual(course, date);
		List<TrainingResponseDto> trainings = new ArrayList<TrainingResponseDto>();
		for (Batch batch : list) {
			TrainingResponseDto trainingResponseDto = new TrainingResponseDto();

			BeanUtils.copyProperties(batch, trainingResponseDto);
			trainings.add(trainingResponseDto);
		}

		if (list.isEmpty()) {
			throw new DataNotFoundException("Batches are not there");

		} else {
			return trainings;
		}
	}

}