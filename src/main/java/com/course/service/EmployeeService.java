package com.course.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.dto.EnrollResponseDto;
import com.course.exception.DataNotFoundException;
import com.course.model.Batch;
import com.course.model.Employee;
import com.course.repository.BatchRepository;
import com.course.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	BatchRepository trainingRepository;

	public EnrollResponseDto enrollCourse(Long employeeId, Long batchId) {
		LocalDate localDate = LocalDate.now();
		EnrollResponseDto enrollResponseDto = new EnrollResponseDto();
		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new DataNotFoundException("employee with given id not found"));
		Batch batch = trainingRepository.findById(batchId)
				.orElseThrow(() -> new DataNotFoundException("training with given id not found"));
		if (batch.getStartDate().compareTo(localDate) > 0 && employee.getRedgStatus().equalsIgnoreCase("Available")) {
			List<Employee> employees = new ArrayList<>();
			employee.setRedgStatus("not Avilable");
			employeeRepository.save(employee);
			employees.add(employee);
			batch.setEmployee(employees);
			trainingRepository.save(batch);

			BeanUtils.copyProperties(batch, enrollResponseDto);
			enrollResponseDto.setCourseName(batch.getCourse().getCourseName());
			enrollResponseDto.setMessage("successfully enrolled in " + batch.getCourse().getCourseName()
					+ " with batch name " + batch.getBatchname());
		} else {
			enrollResponseDto.setMessage("enrolling is not possible");
		}
		return enrollResponseDto;
	}
}